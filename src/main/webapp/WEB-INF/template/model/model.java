package #(table.model_package);

import cc.jweb.boot.annotation.Column;
import com.jfinal.plugin.activerecord.IBean;
import io.jboot.db.annotation.Table;
import io.jboot.db.model.JbootModel;
import java.util.*;
import java.lang.*;
import java.math.*;

/**
 * #(table.table_name)
 * 该模型代码由代码生成器自动生成
 *
 * @generateDate #date(currentDatetime, "yyyy-MM-dd HH:mm:ss")
 * @since 1.0
 * @author imlzw
 * @source 阿呆极速开发平台 adai.jweb.cc
 */
@Table(tableName = "#(table.table_key)", primaryKey = "#(table.primaryKey)")
public class #(table.modelName) extends JbootModel<#(table.modelName)> implements IBean  {
	private static final long serialVersionUID = -#(currentTimeMillis)L;
	public static final #(table.modelName) dao = new #(table.modelName)().dao();
    // 自动生成模型字段列表
#for(field : table.fields) #set(fieldVarName=lineToHump(field.field_key, false))
    // #(field.field_name)
    @Column(field="#(field.field_key)")
    private #(getJavaType(field)) #(fieldVarName);
#end
    // 自动生成getter与setter方法
#for(field : table.fields) #set(fieldVarName=lineToHump(field.field_key, false),fieldName=lineToHump(field.field_key, true),javaType=getJavaType(field))
    /**
     *  获取#(field.field_name)
     */
    public #(javaType) get#(fieldName)(){
        #if(javaType.equals("Boolean"))Object value = get("#(field.field_key)");
        if(value instanceof Integer){
            Integer b = (Integer)get("#(field.field_key)");
            return b != null && b == 1;
        }
        return (Boolean)value;#else return (#(javaType))get("#(field.field_key)");#end
    }

    /**
     *  设置#(field.field_name)
     */
    public #(table.modelName) set#(fieldName)(#(javaType) #(fieldVarName)) {
        #if(javaType.equals("Boolean"))set("#(field.field_key)", #(fieldVarName) ? 1 : 0) #else set("#(field.field_key)", #(fieldVarName))#end;
        return this;
    }
#end
}
