;(function($, window, document,undefined){
	"use strict";

	$.fn.myForm = function(options,param){
		var me = this;
		if (typeof options == 'string'){
			var method = $.fn.myForm.methods[options];
			if (method){
				/* 调用方法 */
				return method(this, param);
			}
		}
		options = options || {};
		return this.each(function(){
			/* 执行实例化 */
			if(!$.data(this, "datatable-api"))
				$.fn.myForm.methods.init(this,options);
		});
	}
	
	$.fn.myForm.methods = {
		/* 初始化表单 */
		init:function(dom,options){
			var $form = $(dom);
			/**
			 * 定义默认验证
			 */
			var validate = {
				/* 忽略 */
				ignore : 'input[type=hidden], .select2-search__field',
				errorClass : 'validation-error-label',
				successClass : 'validation-valid-label',
				highlight : function(element, errorClass) {
					$(element).removeClass(errorClass);
				},
				unhighlight : function(element, errorClass) {
					$(element).removeClass(errorClass);
				},
				// Different components require proper error label placement
				errorPlacement : function(error, element) {
					// Styled checkboxes, radios, bootstrap switch
					if (element.parents('div').hasClass("checker")
							|| element.parents('div').hasClass("choice")
							|| element.parent().hasClass(
									'bootstrap-switch-container')) {
						if (element.parents('label')
								.hasClass('checkbox-inline')
								|| element.parents('label').hasClass(
										'radio-inline')) {
							error.appendTo(element.parent().parent().parent()
									.parent());
						} else {
							error.appendTo(element.parent().parent().parent()
									.parent().parent());
						}
					}
					// Unstyled checkboxes, radios
					else if (element.parents('div').hasClass('checkbox')
							|| element.parents('div').hasClass('radio')) {
						error.appendTo(element.parent().parent().parent());
					}
					// Input with icons and Select2
					else if (element.parents('div').hasClass('has-feedback')
							|| element.hasClass('select2-hidden-accessible')) {
						error.appendTo(element.parent());
					}
					// Inline checkboxes, radios
					else if (element.parents('label').hasClass(
							'checkbox-inline')
							|| element.parents('label')
									.hasClass('radio-inline')) {
						error.appendTo(element.parent().parent());
					}
					// Input group, styled file input
					else if (element.parent().hasClass('uploader')
							|| element.parents().hasClass('input-group')) {
						error.appendTo(element.parent().parent());
					}else if(element.hasClass('combo-f')){
						error.appendTo(element.parent());
					} else {
						error.insertAfter(element);
					}
				},
				validClass : "validation-valid-label",
				success : function(label) {
					label.remove();
					//label.addClass("validation-valid-label").text("验证通过 :)")
				},
				submitHandler:function(f){
					var handler = true;
					/*  */
					if(options.submitHandler){
						handler = options.submitHandler(f);
					}
					handler = handler == undefined ? true : handler;
					if(handler){
						var $f = $(f),params = $f.serialize();
						$f.ajaxSubmit({
							type:'post',
							url:options.url,
							dataType:'json',
							beforeSubmit:options.beforeSubmit||function(data,form){},
							success: options.success||function(result){},
							error : function(XmlHttpRequest, textStatus, errorThrown){
								alert(textStatus);
							}
						});
						return false;
					}
				}
			};
			/* 重写属性 */
			validate = $.extend( true,{}, validate, options.validate||{});
			/* 设置提交方法 */
			/**
			 * 提交表单事件
			 */
			if(options.btn){
				$(options.btn).click(function(){
					$form.validate(validate);
					$form.submit();
				});
			}
		},
		/* 重设表单 */
		reset:function(dom,params){
			dom[0].reset();
			dom.find('select').trigger('change');
		},
		/* 表单数据绑定 */
		load:function(dom,params){
			if(typeof params == 'string'){
				params = {
					url:params
				};
			}
			params['callback'] = params['success']||function(){}
			params['success'] = function(result){
				$(dom).myForm('formBackfill',result.data);
				if(params.callback){
					params.callback(result);
				}
			}
			$.ajax(params);
		},
		/* 基础回填 */
		formBackfill:function(dom,data){
			var $f = $(dom);
			var params = $f.serializeArray();
			/* 修复radio checkbox 在未选中的情况下未加入params问题 */
			var _tag = {};
			$f.find('input[type="radio"],input[type="checkbox"]').each(function(){
				_tag[$(this).attr('name')] = '';
			});
			for(var n in _tag){
				params.push({name:n,value:''});
			}
			/**/
			$.each(params,function(i,o){
				var $tag = $('*[name="'+o.name+'"]');
				$tag.each(function(){
					var $this = $(this),tag = $(this)[0];
					if(data[o.name]!=undefined){
						var nodeName = $tag[0].nodeName;
						if("INPUT" === nodeName){
							if("radio" === tag.type || "checkbox" === tag.type){
								if(data[o.name] == tag.value){
									$this.attr('checked',true);
								}
							}else{
								$tag.val(data[o.name]);
							}
						}else if("SELECT" === nodeName){
							$tag.find('option').each(function(i){
								if($(this).val() == data[o.name]){
									$tag.get(0).selectedIndex = i;
									$tag.get(0).options[i].selected = true;  
								}
							});
						}else{
							$tag.val(data[o.name]);
						}
					}
					
					
//					var $this = $(this),tag_name = $(this)[0].tagName;
//					if(data[o.name] && data[o.name]!=null){
//						var nodeName = $tag[0].nodeName;
//						if("INPUT" === nodeName){
//							if("radio" === $tag[0]){}
//							console.log($tag);
//							$tag.val(data[o.name]);
//						}else if("SELECT" === nodeName){
//							$tag.find('option').each(function(i){
//								if($(this).val() == data[o.name]){
//									$tag.get(0).selectedIndex = i;
//									$tag.get(0).options[i].selected = true;  
//								}
//							});
//						}else{
//							$tag.val(data[o.name]);
//						}
//					}
				});
			});
		}
	};
	
})(jQuery, window, document);