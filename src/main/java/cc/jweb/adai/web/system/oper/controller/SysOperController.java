/*
 * Copyright (c) 2020-2021 imlzw@vip.qq.com jweb.cc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package cc.jweb.adai.web.system.oper.controller;

import cc.jweb.adai.web.system.generator.utils.GeneratorUtils;
import cc.jweb.adai.web.system.log.service.SysLogService;
import cc.jweb.adai.web.system.oper.model.SysOper;
import cc.jweb.adai.web.system.sys.model.SysLog;
import cc.jweb.boot.common.exception.ParameterValidationException;
import cc.jweb.boot.common.lang.Result;
import cc.jweb.boot.controller.JwebController;
import cc.jweb.boot.db.Db;
import cc.jweb.boot.security.annotation.Logical;
import cc.jweb.boot.security.annotation.RequiresPermissions;
import cc.jweb.boot.utils.lang.StringUtils;
import com.jfinal.core.ActionKey;
import com.jfinal.core.NotAction;
import io.jboot.web.controller.annotation.RequestMapping;

import java.util.Date;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * 操作权限
 * 该控制器代码由阿呆代码生成器自动生成
 *
 * @author imlzw
 * @generateDate 2020-10-28 18:15:56
 * @source 阿呆极速开发平台 adai.imlzw.cn
 * @since 1.0
 */
@RequiresPermissions("generator:12:view")
@RequestMapping(value = "/system/oper", viewPath = "/WEB-INF/views/cc/jweb/web/system/oper/gid_12")
public class SysOperController extends JwebController {

    public void index() {
        render("index.html");
    }

    /**
     * 编辑页面
     */
    @RequiresPermissions("generator:12:edit")
    public void editPage() {
        String id = getPara("oper_id");
        SysOper sysOper = null;
        if (id != null) {
            sysOper = SysOper.dao.findById(id);
            if (sysOper == null) {
                sysOper = new SysOper();
            }
        }
        if (sysOper == null) {
            sysOper = new SysOper();
        }
        setAttr("detail", sysOper);
        keepPara();
        render("edit.html");
    }

    /**
     * 加载分页列表数据
     */
    @RequiresPermissions("generator:12:list")
    public void list() {
        Map<String, Object> params = getPageParamsPlus();
        params.put("_filters_", GeneratorUtils.parseFilterParams(params));
        Object[] values = getParaValues("values");
        Result result = new Result(false, "未知异常！");
        result.set("code", 400);
        if (StringUtils.isNotBlank(values)) { // formSelects的回显接口
            params.put("_values_", values);
            String valueKey = getPara("valueKey");
            params.put("_valueKey_", valueKey != null ? valueKey : "oper_id");
            result.setListData(Db.find(Db.getSqlPara("gid-12-tno-1-cc.jweb.adai.web.system.oper.sys_oper.queryPageList", params)));
            result.setSuccess(true);
            result.set("code", 0);
        } else {
            result = Db.paginate("gid-12-tno-1-cc.jweb.adai.web.system.oper.sys_oper.queryPageList", "gid-12-tno-1-cc.jweb.adai.web.system.oper.sys_oper.count", params);
            result.set("count", result.get(Result.LIST_TOTAL_KEY));
            result.setSuccess(true);
            result.set("code", 0);
        }
        renderJson(result);
    }

    /**
     * 保存用户信息（新增与修改）
     */
    @RequiresPermissions(value = {"generator:12:add", "generator:12:edit"}, logical = Logical.OR)
    public void save() {
        SysOper columnModel = getColumnModel(SysOper.class);
        SysOper oldModel = null;
        Object id = columnModel.get("oper_id");
        if (id != null) {
            oldModel = SysOper.dao.findById(id);
        }
        if (oldModel != null) { // 编辑
            fixModel(columnModel);
            verifyModel(columnModel);
            columnModel.update();
        } else { // 新增

            columnModel.set("create_datetime", new Date());
            fixModel(columnModel);
            verifyModel(columnModel);
            columnModel.save();
        }
        SysLogService.service.setSyslog(SysLog.STATUS_SUCCESS ," 保存操作权限【" + columnModel.getOperName()+ "】成功！");
        renderJson(new Result(true, "保存操作权限信息成功！"));
    }

    /**
     * 修正模型数据
     * 避免模型非空字段 无数据时，保存或者更新时异常
     */
    @NotAction
    private void fixModel(SysOper columnModel) {
        Set<String> attrs = columnModel._getAttrsEntrySet().stream().map(stringObjectEntry -> stringObjectEntry.getKey()).collect(Collectors.toSet());
        if (attrs.contains("oper_id") && columnModel.get("oper_id") == null) {
            // 字段默认值为：NULL
            columnModel.remove("oper_id");
        }

        if (attrs.contains("oper_name") && columnModel.get("oper_name") == null) {
            // 字段默认值为：NULL
            columnModel.remove("oper_name");
        }

        if (attrs.contains("oper_key") && columnModel.get("oper_key") == null) {
            // 字段默认值为：NULL
            columnModel.remove("oper_key");
        }

        if (attrs.contains("oper_pid") && columnModel.get("oper_pid") == null) {
            // 字段默认值为：0
            columnModel.set("oper_pid", "0");
        }

        if (attrs.contains("oper_type") && columnModel.get("oper_type") == null) {
            // 字段默认值为：0
            columnModel.set("oper_type", "0");
        }

        if (attrs.contains("permission_key") && columnModel.get("permission_key") == null) {
            // 字段默认值为：NULL
            columnModel.remove("permission_key");
        }

        if (attrs.contains("status") && columnModel.get("status") == null) {
            // 字段默认值为：0
            columnModel.set("status", "0");
        }

        if (attrs.contains("order_no") && columnModel.get("order_no") == null) {
            // 字段默认值为：100
            columnModel.set("order_no", "100");
        }

        if (attrs.contains("create_datetime") && columnModel.get("create_datetime") == null) {
            // 字段默认值为：NULL
            columnModel.remove("create_datetime");
        }

    }

    /**
     * 验证数据
     * 对模型中的字段数据进行校验
     */
    @NotAction
    private void verifyModel(SysOper columnModel) {
        Set<String> attrs = columnModel._getAttrsEntrySet().stream().map(stringObjectEntry -> stringObjectEntry.getKey()).collect(Collectors.toSet());
        if (attrs.contains("oper_name") && columnModel.getOperName() != null && columnModel.getOperName().length() > 32) {
            throw new ParameterValidationException("操作名称的长度不能超过32个字符！");
        }
        if (attrs.contains("oper_key") && columnModel.getOperKey() != null && columnModel.getOperKey().length() > 32) {
            throw new ParameterValidationException("关键字的长度不能超过32个字符！");
        }
        if (attrs.contains("oper_path") && columnModel.getOperPath() != null && columnModel.getOperPath().length() > 128) {
            throw new ParameterValidationException("操作路径的长度不能超过128个字符！");
        }
        if (attrs.contains("permission_key") && columnModel.getPermissionKey() != null && columnModel.getPermissionKey().length() > 128) {
            throw new ParameterValidationException("权限标识的长度不能超过128个字符！");
        }
        if (attrs.contains("oper_icon") && columnModel.getOperIcon() != null && columnModel.getOperIcon().length() > 32) {
            throw new ParameterValidationException("图标的长度不能超过32个字符！");
        }
    }

    /**
     * 删除记录
     */
    @RequiresPermissions("generator:12:del")
    public void delete() {
        String[] ids = getParaValues("ids");
        if (ids == null || ids.length <= 0) {
            renderJson(new Result(true, "删除成功！"));
            return;
        }
        boolean b = true;
        for (String id : ids) {
            b = b & SysOper.dao.deleteById(id);
        }
        SysLogService.service.setSyslog(b ? SysLog.STATUS_SUCCESS : SysLog.STATUS_FAILURE, "删除操作权限【id:" + StringUtils.join(ids, ",") + "】" + (b ? "成功" : "失败") + " !");
        renderJson(new Result(b, b ? "删除成功！" : "删除失败！"));
    }

    // ---- 字段关联接口 ----

    /**
     * oper_pid 字段关联表列表接口
     */
    @ActionKey("/system/oper/oper_pid/sql/list")
    public void operPidSqlList() {
        Map<String, Object> params = getPageParamsPlus();
        params.put("_filters_", GeneratorUtils.parseFilterParams(params));
        Object[] values = getParaValues("values");
        Result result = new Result(false, "未知异常！");
        result.set("code", 400);
        if (StringUtils.isNotBlank(values)) { // formSelects的回显接口
            params.put("oper_ids", values);
            result.setListData(Db.find(Db.getSqlPara("gid-12-tno-1-cc.jweb.adai.web.system.oper.sys_oper.oper_pid_sql_list", params)));
            result.setSuccess(true);
            result.set("code", 0);
        } else {
            result = Db.paginate("gid-12-tno-1-cc.jweb.adai.web.system.oper.sys_oper.oper_pid_sql_list", "gid-12-tno-1-cc.jweb.adai.web.system.oper.sys_oper.oper_pid_sql_count", params);
            result.set("count", result.get(Result.LIST_TOTAL_KEY));
            result.setSuccess(true);
            result.set("code", 0);
        }
        renderJson(result);
    }

}