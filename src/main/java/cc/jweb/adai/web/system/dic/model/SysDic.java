/*
 * Copyright (c) 2020-2021 imlzw@vip.qq.com jweb.cc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package cc.jweb.adai.web.system.dic.model;

import cc.jweb.boot.annotation.Column;
import com.jfinal.plugin.activerecord.IBean;
import io.jboot.db.annotation.Table;
import io.jboot.db.model.JbootModel;

import java.util.Date;

/**
 * 数据字典
 * 该模型代码由代码生成器自动生成
 *
 * @author imlzw
 * @generateDate 2020-10-15 14:36:47
 * @source 阿呆极速开发平台 adai.imlzw.cn
 * @since 1.0
 */
@Table(tableName = "sys_dic", primaryKey = "dic_id")
public class SysDic extends JbootModel<SysDic> implements IBean {
    public static final SysDic dao = new SysDic().dao();
    private static final long serialVersionUID = -1602743807526L;
    // 自动生成模型字段列表
    // 字典编号
    @Column(field = "dic_id")
    private Integer dicId;

    // 上级编号
    @Column(field = "dic_pid")
    private Integer dicPid;

    // 字典代码
    @Column(field = "dic_code")
    private String dicCode;

    // 字典名称
    @Column(field = "dic_name")
    private String dicName;

    // 字典值
    @Column(field = "dic_value")
    private String dicValue;

    // 创建时间
    @Column(field = "create_datetime")
    private Date createDatetime;

    // 是否启用
    @Column(field = "is_enable")
    private Boolean isEnable;

    // 排序号
    @Column(field = "order_no")
    private Integer orderNo;

    // 自动生成getter与setter方法

    /**
     * 获取字典编号
     */
    public Integer getDicId() {
        return (Integer) get("dic_id");
    }

    /**
     * 设置字典编号
     */
    public SysDic setDicId(Integer dicId) {
        set("dic_id", dicId);
        return this;
    }

    /**
     * 获取上级编号
     */
    public Integer getDicPid() {
        return (Integer) get("dic_pid");
    }

    /**
     * 设置上级编号
     */
    public SysDic setDicPid(Integer dicPid) {
        set("dic_pid", dicPid);
        return this;
    }

    /**
     * 获取字典代码
     */
    public String getDicCode() {
        return (String) get("dic_code");
    }

    /**
     * 设置字典代码
     */
    public SysDic setDicCode(String dicCode) {
        set("dic_code", dicCode);
        return this;
    }

    /**
     * 获取字典名称
     */
    public String getDicName() {
        return (String) get("dic_name");
    }

    /**
     * 设置字典名称
     */
    public SysDic setDicName(String dicName) {
        set("dic_name", dicName);
        return this;
    }

    /**
     * 获取字典值
     */
    public String getDicValue() {
        return (String) get("dic_value");
    }

    /**
     * 设置字典值
     */
    public SysDic setDicValue(String dicValue) {
        set("dic_value", dicValue);
        return this;
    }

    /**
     * 获取创建时间
     */
    public Date getCreateDatetime() {
        return (Date) get("create_datetime");
    }

    /**
     * 设置创建时间
     */
    public SysDic setCreateDatetime(Date createDatetime) {
        set("create_datetime", createDatetime);
        return this;
    }

    /**
     * 获取是否启用
     */
    public Boolean getIsEnable() {
        Object value = get("is_enable");
        if (value instanceof Integer) {
            Integer b = (Integer) get("is_enable");
            return b != null && b == 1;
        }
        return (Boolean) value;
    }

    /**
     * 设置是否启用
     */
    public SysDic setIsEnable(Boolean isEnable) {
        set("is_enable", isEnable ? 1 : 0);
        return this;
    }

    /**
     * 获取排序号
     */
    public Integer getOrderNo() {
        return (Integer) get("order_no");
    }

    /**
     * 设置排序号
     */
    public SysDic setOrderNo(Integer orderNo) {
        set("order_no", orderNo);
        return this;
    }

}