/*
 * Copyright (c) 2020-2021 imlzw@vip.qq.com jweb.cc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package cc.jweb.adai.web.system.generator.model.code;

import cc.jweb.adai.web.system.generator.model.TableModel;
import cc.jweb.boot.utils.gson.GsonUtils;

import java.util.List;

/**
 * 表代码模型
 */
public class TableCodeModel extends TableModel {

    private List<FieldCodeModel> fieldModelList;

    public List<FieldCodeModel> getFieldModelList() {
        return fieldModelList;
    }

    public TableCodeModel setFieldModelList(List<FieldCodeModel> fieldModelList) {
        this.fieldModelList = fieldModelList;
        return this;
    }

    @Override
    public String toJson() {
        return GsonUtils.get().toJson(this);
    }

    public void init(TableModel source) {
        this.setTableId(source.getTableId());
        this.setTableName(source.getTableName());
        this.setTableKey(source.getTableKey());
        this.setModelPackage(source.getModelPackage());
        this.setCreateDatetime(source.getCreateDatetime());
    }
}
