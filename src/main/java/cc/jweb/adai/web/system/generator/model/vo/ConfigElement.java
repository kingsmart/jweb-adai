/*
 * Copyright (c) 2020-2021 imlzw@vip.qq.com jweb.cc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package cc.jweb.adai.web.system.generator.model.vo;

import java.util.ArrayList;
import java.util.List;

/**
 * 配置元素类
 *
 * @author linzhiwei
 */
public class ConfigElement {

    //配置中文名
    private String configZhName;
    //配置名称（英文）
    private String configName;
    // 配置值
    private String configValue;
    // 可选值
    private List<String[]> options;
    // 是否非空
    private boolean notNull = true;
    // 是否可自定义值
    private boolean canCustomValue = true;
    // 备注
    private String desc;

    public ConfigElement() {

    }

    public ConfigElement(String configZhName, String configName, String configValue) {
        this.configZhName = configZhName;
        this.configName = configName;
        this.configValue = configValue;
    }

    public ConfigElement(String configZhName, String configName, String configValue, String desc) {
        this(configZhName, configName, configValue);
        this.desc = desc;
    }

    public ConfigElement(String configZhName, String configName, String configValue, boolean notNull, boolean canCustomValue) {
        this.configZhName = configZhName;
        this.configName = configName;
        this.configValue = configValue;
        this.notNull = notNull;
        this.canCustomValue = canCustomValue;
    }

    public ConfigElement(String configZhName, String configName, String configValue, List<String[]> options, boolean canCustomValue) {
        this.configZhName = configZhName;
        this.configName = configName;
        this.configValue = configValue;
        this.options = options;
        this.canCustomValue = canCustomValue;
    }

    /**
     * 获取配置名称（英文）
     *
     * @return
     */
    public String getConfigName() {
        return configName;
    }

    /**
     * 设置配置名称（英文）
     *
     * @param configName
     */
    public void setConfigName(String configName) {
        this.configName = configName;
    }

    /**
     * 获取配置值
     *
     * @return
     */
    public String getConfigValue() {
        return configValue;
    }

    /**
     * 设置配置值
     *
     * @param configValue
     */
    public void setConfigValue(String configValue) {
        this.configValue = configValue;
    }

    /**
     * 获取配置中文名
     *
     * @return
     */
    public String getConfigZhName() {
        return configZhName;
    }

    /**
     * 设置配置中文名
     *
     * @param configZhName
     */
    public void setConfigZhName(String configZhName) {
        this.configZhName = configZhName;
    }

    /**
     * 获取可选项列表
     *
     * @return
     */
    public List<String[]> getOptions() {
        return options;
    }

    /**
     * 设置可选项列表
     *
     * @param options
     */
    public void setOptions(List<String[]> options) {
        this.options = options;
    }

    /**
     * 获取是否可自定义值
     *
     * @return
     */
    public boolean isCanCustomValue() {
        return canCustomValue;
    }

    /**
     * 设置是否可自定义值
     *
     * @param canCustomValue
     */
    public void setCanCustomValue(boolean canCustomValue) {
        this.canCustomValue = canCustomValue;
    }

    /**
     * 是否非空
     *
     * @return
     */
    public boolean isNotNull() {
        return notNull;
    }

    /**
     * 设置是否非空
     *
     * @param notNull
     */
    public void setNotNull(boolean notNull) {
        this.notNull = notNull;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    /**
     * 添加可选项
     *
     * @param title 标题
     * @param value 值
     */
    public void addOption(String title, String value) {
        if (title != null && value != null && title.trim().length() > 0 && value.trim().length() > 0) {
            if (this.options == null) {
                this.options = new ArrayList<String[]>();
            }
            this.options.add(new String[]{title, value});
        }
    }
}
