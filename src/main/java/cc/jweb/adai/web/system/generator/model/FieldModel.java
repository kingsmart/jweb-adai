/*
 * Copyright (c) 2020-2021 imlzw@vip.qq.com jweb.cc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package cc.jweb.adai.web.system.generator.model;

import cc.jweb.boot.annotation.Column;
import cc.jweb.boot.utils.gson.GsonUtils;
import com.jfinal.plugin.activerecord.IBean;
import cc.jweb.adai.web.system.generator.model.vo.FieldConfig;
import io.jboot.db.annotation.Table;
import io.jboot.db.model.JbootModel;
import org.apache.commons.collections4.MapUtils;

import java.util.Date;

/**
 * 字段模型
 */
@Table(tableName = "sys_field_model", primaryKey = "field_id")
public class FieldModel<F extends FieldModel> extends JbootModel<FieldModel<FieldModel>> implements IBean {
    public static final FieldModel dao = new FieldModel<FieldModel>().dao();
    private static final long serialVersionUID = -2931621316918877627L;

    @Column(field = "field_id")
    private Integer fieldId;

    @Column(field = "field_name")
    private String fieldName;

    @Column(field = "field_key")
    private String fieldKey;

    @Column(field = "field_type")
    private String fieldType;

    @Column(field = "field_length")
    private Integer fieldLength;

    @Column(field = "decimal_point")
    private Integer decimalPoint; // 小数点几位

    @Column(field = "is_nullable")
    private boolean isNullable; // 是否可空

    @Column(field = "is_primary")
    private boolean isPrimary; // 是否是主键

    @Column(field = "create_datetime")
    private Date createDatetime;

    @Column(field = "field_config")
    private String fieldConfig;

    @Column(field = "field_default")
    private String fieldDefault;

    @Column(field = "field_order")
    private Integer fieldOrder;

    @Column(field = "table_id")
    private Integer tableId; // 表外键关联

    @Column(field = "order_no")
    private Integer orderNo; // 表外键关联

    public Integer getFieldId() {
        return get("field_id");
    }

    public F setFieldId(Integer fieldId) {
        set("field_id", fieldId);
        return (F) this;
    }

    public String getFieldName() {
        return get("field_name");
    }

    public F setFieldName(String fieldName) {
        set("field_name", fieldName);
        return (F) this;
    }

    public String getFieldKey() {
        return get("field_key");
    }

    public F setFieldKey(String fieldKey) {
        set("field_key", fieldKey);
        return (F) this;
    }

    public String getFieldType() {
        return get("field_type");
    }

    public F setFieldType(String fieldType) {
        set("field_type", fieldType);
        return (F) this;
    }

    public Integer getFieldLength() {
        return get("field_length");
    }

    public F setFieldLength(Integer fieldLength) {
        set("field_length", fieldLength);
        return (F) this;
    }

    public Integer getDecimalPoint() {
        return get("decimal_point");
    }

    public F setDecimalPoint(Integer decimalPoint) {
        set("decimal_point", decimalPoint);
        return (F) this;
    }

    public boolean isNullable() {
        return MapUtils.getBoolean(this._getAttrs(), "is_nullable");
    }

    public F setNullable(boolean nullable) {
        set("is_nullable", nullable ? 1 : 0);
        return (F) this;
    }

    public boolean isPrimary() {
        return MapUtils.getBoolean(this._getAttrs(), "is_primary");
    }

    public F setPrimary(boolean primary) {
        set("is_primary", primary ? 1 : 0);
        return (F) this;
    }

    public Date getCreateDatetime() {
        return get("create_datetime");
    }

    public F setCreateDatetime(Date createDatetime) {
        set("create_datetime", createDatetime);
        return (F) this;
    }

    public String getFieldConfig() {
        return get("field_config");
    }

    public F setFieldConfig(String fieldConfig) {
        set("field_config", fieldConfig);
        return (F) this;
    }

    public String getFieldDefault() {
        return get("field_default");
    }

    public F setFieldDefault(String fieldDefault) {
        set("field_default", fieldDefault);
        return (F) this;
    }

    public Integer getFieldOrder() {
        return get("field_order");
    }

    public F setFieldOrder(Integer fieldOrder) {
        set("field_order", fieldOrder);
        return (F) this;
    }

    public Integer getTableId() {
        return get("table_id");
    }

    public F setTableId(Integer tableId) {
        set("table_id", tableId);
        return (F) this;
    }

    public Integer getOrderNo() {
        return get("order_no");
    }

    public F setOrderNo(Integer orderNo) {
        set("order_no", orderNo);
        return (F) this;
    }

    public FieldConfig toFieldConfig() {
        String fieldConfig = getFieldConfig();
        FieldConfig fieldConfigObject = null;
        if (fieldConfig != null) {
            fieldConfigObject = GsonUtils.get().fromJson(fieldConfig, FieldConfig.class);

        }
        if (fieldConfigObject == null) {
            fieldConfigObject = new FieldConfig();
        }
        return fieldConfigObject;
    }
}
