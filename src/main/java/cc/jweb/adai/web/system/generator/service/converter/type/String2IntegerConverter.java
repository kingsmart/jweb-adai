/*
 * Copyright (c) 2020-2021 imlzw@vip.qq.com jweb.cc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package cc.jweb.adai.web.system.generator.service.converter.type;

import cc.jweb.adai.web.system.generator.model.vo.Config;
import cc.jweb.adai.web.system.generator.model.vo.ConfigElement;

import java.util.List;

/**
 * 字段串转整形类型转换器
 * @author linzhiwei
 *
 */
public class String2IntegerConverter implements Converter {

	@Override
	public Integer convert(Object object, Config config) throws Exception {
		Integer integer  = null;
		if(object!=null){
			integer = Integer.parseInt(object.toString());
		}
		return integer;
	}

	@Override
	public String getConverterDesc() {
		return "字段串转整形类型转换器";
	}

	@Override
	public List<ConfigElement> getConfigParams() {
		return null;
	}

}
