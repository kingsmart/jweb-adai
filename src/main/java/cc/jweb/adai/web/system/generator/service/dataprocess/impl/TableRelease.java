//package cc.jweb.adai.web.generator.service.dataprocess.impl;
//
//import com.ndasec.base.common.lang.StringUtils;
//import cc.jweb.adai.web.generator.model.vo.Config;
//import cc.jweb.adai.web.generator.model.vo.ConfigElement;
//import cc.jweb.adai.web.generator.service.dataprocess.DataProcess;
//
//import java.util.ArrayList;
//import java.util.HashSet;
//import java.util.List;
//
///**
// * 表关联处理
// *
// * @author 志伟
// *
// */
//public class TableRelease implements DataProcess {
//
//	private List<ConfigElement> configElementList = null;
//	private StringSpliter stringSpliter  = null;
//	private StringRegexReplace stringReplace = null;
//
//	public TableRelease(){
//		configElementList = new ArrayList<ConfigElement>();
//		stringSpliter  = new StringSpliter();
//		stringReplace = new StringRegexReplace();
//		configElementList.addAll(stringSpliter.getConfigParams());
//		configElementList.addAll(stringReplace.getConfigParams());
//	}
//
//	@SuppressWarnings("unchecked")
//	@Override
//	public List<String> process(Object object, Config config)  throws Exception{
//		List<String> process = stringSpliter.process(object, config);
//		if(process!=null){
//			for(int i=0;i<process.size();i++){
//				process.set(i, stringReplace.process(process.get(i),config));
//			}
//		}
//		//排空排重
//		if(StringUtils.isNotBlank(process)){
//			List<String> newResult = new ArrayList<String>();
//			HashSet<String> set = new HashSet<String>();
//			for(String str : process){
//				if(str!=null&&!set.contains(str)){
//					newResult.add(str);
//					set.add(str);
//				}
//			}
//			return newResult;
//		}
//		return null;
//	}
//
//	public String getConfigDesc() {
//		return "/*字符串正则分割再替换数据处理类，前分割后，每个字符串再替换处理！*/";
//	}
//
//	public List<ConfigElement> getConfigParams() {
//		return configElementList;
//	}
//
//	public static void main(String[] args) {
//		Config config = new Config();
//		config.addConfig(new ConfigElement("", StringSpliter.SPLIT_REGEX, "[,]"));
//		config.addConfig(new ConfigElement("", StringRegexReplace.MATCH_REGEX, "(.*):(.*):(.*)"));
//		config.addConfig(new ConfigElement("", StringRegexReplace.REPLACEMENT, "$2"));
//		try {
//			List<String> process = new TableRelease().process("aaaa:bbbb:cccc,dddd:eeee:ffff", config);
//			System.out.println(process);
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//	}
//}
