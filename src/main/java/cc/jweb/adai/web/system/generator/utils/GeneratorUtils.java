/*
 * Copyright (c) 2020-2021 imlzw@vip.qq.com jweb.cc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package cc.jweb.adai.web.system.generator.utils;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

/**
 * 生成器工具集
 */
public class GeneratorUtils {

    /**
     * 根据包名生成视图路径
     *
     * @param packageName
     * @return
     */
    public static String generatViewPath(String packageName) {
        return File.separator + "WEB-INF" + File.separator + "views" + File.separator + packageName.replace(".", File.separator);
    }

    /**
     * 根据包名生成包相对路径
     *
     * @return
     */
    public static String generatPackagePath(String packageName) {
        return "/" + packageName.replace(".", "/");
    }


    /**
     * 根据列表查询 sql 获取 数量统计sql
     *
     * @param listSql
     * @return
     */
    public static String getCountSql(String listSql) {
        Pattern selectCompile = Pattern.compile("select(.*)?from", Pattern.CASE_INSENSITIVE);
        String countSql = selectCompile.matcher(listSql).replaceFirst("select count(1) from");
        return countSql;
    }


    /**
     * 解析出参数里面有类似格式为： {field}__op__{searchMethod} 的参数
     *
     * @param params
     * @return List<String [ ]> [i][0]=fieldKey,[i][1]=searchMethod,[i][2]=fieldValue
     */
    public static List<String[]> parseFilterParams(Map<String, Object> params) {
        List<String[]> opField = new ArrayList<String[]>();
        for (String key : params.keySet()) {
            int opIndex = key.indexOf("__op__");
            if (opIndex > 0 && opIndex < key.length() - 1) {
                String searchField = key.substring(0, opIndex);
                if (checkField(searchField)) {
                    String searchMethod = key.substring(opIndex + 6);
                    String searchValue = (String) params.get(key);
                    if (searchMethod.indexOf("range") >= 0) {
                        if (searchValue != null) {
                            String[] values = searchValue.split(" - ");
                            if (values.length > 1) {
                                if ("open_range".equals(searchMethod)) {
                                    opField.add(new String[]{searchField, "gt", values[0]});
                                    opField.add(new String[]{searchField, "lt", values[1]});
                                } else {
                                    opField.add(new String[]{searchField, "gteq", values[0]});
                                    opField.add(new String[]{searchField, "lteq", values[1]});
                                }
                            }
                        }
                    } else {
                        opField.add(new String[]{searchField, searchMethod, searchValue});
                    }
                }
            }
        }
        return opField;
    }

    /**
     * 校验字段的合法性,命令规则：字母数字下划线中划线
     *
     * @param searchField
     * @return
     */
    private static boolean checkField(String searchField) {
        return searchField.matches("(?m)^[\\w\\-_]+$");
    }

    /**
     * 解析搜索方法为sql的逻辑运算符
     *
     * @param searchMethod
     * @return
     */
    public static String parseSm2SqlLogOp(String searchMethod) {
        switch (searchMethod) {
            case "eq":
                return "=";
            case "noteq":
                return "<>";
            case "gt":
                return ">";
            case "lt":
                return ">";
            case "gteq":
                return ">=";
            case "lteq":
                return "<=";
            case "like":
                return "like";
            case "notlike":
                return "not like";
            default:
                return "=";
        }
    }

    /**
     * 是否是时间对象
     *
     * @param object
     * @return
     */
    public static boolean isDateObject(Object object) {
        return object instanceof Date;
    }


}
