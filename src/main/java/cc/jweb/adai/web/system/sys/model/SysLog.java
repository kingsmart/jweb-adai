/*
 * Copyright (c) 2020-2021 imlzw@vip.qq.com jweb.cc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package cc.jweb.adai.web.system.sys.model;

import cc.jweb.boot.annotation.Column;
import com.jfinal.plugin.activerecord.IBean;
import io.jboot.db.annotation.Table;
import io.jboot.db.model.JbootModel;

import java.util.Date;

/**
 * 系统日志
 * 该模型代码由代码生成器自动生成
 *
 * @author imlzw
 * @generateDate 2020-09-27 19:22:43
 * @source 阿呆极速开发平台 adai.imlzw.cn
 * @since 1.0
 */
@Table(tableName = "sys_log", primaryKey = "log_id")
public class SysLog extends JbootModel<SysLog> implements IBean {
    public static final SysLog dao = new SysLog().dao();
    private static final long serialVersionUID = -1601205763040L;

    public static final int STATUS_FAILURE = 0;
    public static final int STATUS_SUCCESS = 1;

    // 自动生成模型字段列表
    // 日志状态
    @Column(field = "log_status")
    private Integer logStatus;

    // 日志IP
    @Column(field = "log_ip")
    private String logIp;

    // 日志URL
    @Column(field = "log_url")
    private String logUrl;

    // 日志内容
    @Column(field = "log_content")
    private String logContent;

    // 日志分类
    @Column(field = "log_category")
    private String logCategory;

    // 日志时间
    @Column(field = "log_datetime")
    private Date logDatetime;

    // 日志用户
    @Column(field = "user_id")
    private Integer userId;

    // 日志编号
    @Column(field = "log_id")
    private Long logId;

    // 自动生成getter与setter方法

    /**
     * 获取日志状态
     */
    public Integer getLogStatus() {
        return (Integer) get("log_status");
    }

    /**
     * 设置日志状态
     */
    public SysLog setLogStatus(Integer logStatus) {
        set("log_status", logStatus);
        return this;
    }

    /**
     * 获取日志IP
     */
    public String getLogIp() {
        return (String) get("log_ip");
    }

    /**
     * 设置日志IP
     */
    public SysLog setLogIp(String logIp) {
        set("log_ip", logIp);
        return this;
    }

    /**
     * 获取日志URL
     */
    public String getLogUrl() {
        return (String) get("log_url");
    }

    /**
     * 设置日志URL
     */
    public SysLog setLogUrl(String logUrl) {
        set("log_url", logUrl);
        return this;
    }

    /**
     * 获取日志内容
     */
    public String getLogContent() {
        return (String) get("log_content");
    }

    /**
     * 设置日志内容
     */
    public SysLog setLogContent(String logContent) {
        set("log_content", logContent);
        return this;
    }

    /**
     * 获取日志分类
     */
    public String getLogCategory() {
        return (String) get("log_category");
    }

    /**
     * 设置日志分类
     */
    public SysLog setLogCategory(String logCategory) {
        set("log_category", logCategory);
        return this;
    }

    /**
     * 获取日志时间
     */
    public Date getLogDatetime() {
        return (Date) get("log_datetime");
    }

    /**
     * 设置日志时间
     */
    public SysLog setLogDatetime(Date logDatetime) {
        set("log_datetime", logDatetime);
        return this;
    }

    /**
     * 获取日志用户
     */
    public Integer getUserId() {
        return (Integer) get("user_id");
    }

    /**
     * 设置日志用户
     */
    public SysLog setUserId(Integer userId) {
        set("user_id", userId);
        return this;
    }

    /**
     * 获取日志编号
     */
    public Long getLogId() {
        return (Long) get("log_id");
    }

    /**
     * 设置日志编号
     */
    public SysLog setLogId(Long logId) {
        set("log_id", logId);
        return this;
    }

}