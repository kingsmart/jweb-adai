/*
 * Copyright (c) 2020-2021 imlzw@vip.qq.com jweb.cc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package cc.jweb.adai.web.system.generator.service.converter.type;

import cc.jweb.adai.web.system.generator.model.vo.ConfigElement;
import cc.jweb.adai.web.system.generator.model.vo.Config;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * 字段串转时间类型转换器
 * @author linzhiwei
 *
 */
public class Date2StringConverter implements Converter {
	//时间格式化字符串配置名称
	private static String FORMAT_STRING_CONFIG_NAME = "dateFormatString";
	//配置元素列表
	private List<ConfigElement> configElementList = null;

	public  Date2StringConverter(){
		configElementList = new ArrayList<ConfigElement>();
		configElementList.add(new ConfigElement("时间格式化字符串",FORMAT_STRING_CONFIG_NAME,null));
	}

	@Override
	public String convert(Object object, Config config) throws Exception {
		String string  = null;
		if(object!=null&&object instanceof Date){
			if(config!=null){
				String formatString = config.getConfig(FORMAT_STRING_CONFIG_NAME);
				if(formatString!=null&&formatString.trim().length()>0){
					SimpleDateFormat sdf = new SimpleDateFormat(formatString);
					string = sdf.format((Date)object);
				}
			}
		}
		return string;
	}

	@Override
	public String getConverterDesc() {
		return "时间转字段串类型转换器，转换参数："+FORMAT_STRING_CONFIG_NAME;
	}

	@Override
	public List<ConfigElement> getConfigParams() {
		List<ConfigElement> list = new ArrayList<ConfigElement>();
		list.addAll(configElementList);
		return list;
	}

}
