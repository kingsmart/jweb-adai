/*
 * Copyright (c) 2020-2021 imlzw@vip.qq.com jweb.cc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package cc.jweb.adai.web.system.generator.service.dataprocess.impl;

import cc.jweb.adai.web.system.generator.model.vo.ConfigElement;
import cc.jweb.adai.web.system.generator.service.dataprocess.DataProcess;
import cc.jweb.boot.utils.lang.StringUtils;
import cc.jweb.adai.web.system.generator.model.vo.Config;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;


public class StringSpliter implements DataProcess {

    //分隔的正则表达式配置名称
    public static String SPLIT_REGEX = "splitRegex";
    //配置元素列表
    private List<ConfigElement> configElementList = null;

    public StringSpliter() {
        configElementList = new ArrayList<ConfigElement>();
        configElementList.add(new ConfigElement("分割符(正则)", SPLIT_REGEX, null));
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<String> process(Object object, Config config) throws Exception {
        List<String> splitStrs = new ArrayList<String>();
        if (object != null) {
            String string = object.toString();
            if (string != null && string.trim().length() > 0) {
                String splitRegex = config != null ? config.getConfig(SPLIT_REGEX) : null;
                if (splitRegex != null && splitRegex.trim().length() > 0) {
                    String[] split = string.split(splitRegex);
                    if (split != null && split.length > 0) {
                        for (String v : split) {
                            if (v != null && v.trim().length() > 0) {
                                splitStrs.add(v);
                            }
                        }
                    }
                } else {
                    splitStrs.add(string);
                }
            }
        }
        //排空排重
        if (StringUtils.isNotBlank(splitStrs)) {
            List<String> newResult = new ArrayList<String>();
            HashSet<String> set = new HashSet<String>();
            for (String str : splitStrs) {
                if (str != null && !set.contains(str)) {
                    newResult.add(str);
                    set.add(str);
                }
            }
            return newResult;
        }
        return null;
    }


    @Override
    public String getConfigDesc() {
        return "/*分割字符串为多值，分割符正则配置：{" + SPLIT_REGEX + ":\"\\S\";//正则表达式*/}";
    }

    @Override
    public List<ConfigElement> getConfigParams() {
        return configElementList;
    }

}
