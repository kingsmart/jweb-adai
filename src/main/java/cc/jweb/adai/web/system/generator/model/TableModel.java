/*
 * Copyright (c) 2020-2021 imlzw@vip.qq.com jweb.cc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package cc.jweb.adai.web.system.generator.model;

import cc.jweb.boot.annotation.Column;
import com.jfinal.plugin.activerecord.IBean;
import io.jboot.db.annotation.Table;
import io.jboot.db.model.JbootModel;

import java.util.Date;

/**
 * 表模型
 * 该模型代码由代码生成器自动生成
 *
 * @author imlzw
 * @generateDate 2020-07-22 18:47:05
 * @since 1.0
 */
@Table(tableName = "sys_table_model", primaryKey = "table_id")
public class TableModel extends JbootModel<TableModel> implements IBean {
    public static final TableModel dao = new TableModel().dao();
    private static final long serialVersionUID = -1595414825779L;

    // 表编号
    @Column(field = "table_id")
    private Integer tableId;

    // 表名
    @Column(field = "table_key")
    private String tableKey;

    // 显示名称
    @Column(field = "table_name")
    private String tableName;

    // 模型包名
    @Column(field = "model_package")
    private String modelPackage;

    // 创建时间
    @Column(field = "create_datetime")
    private Date createDatetime;

    // 自动生成getter与setter方法

    /**
     * 获取表编号
     */
    public Integer getTableId() {
        return (Integer) get("table_id");
    }

    /**
     * 设置表编号
     */
    public TableModel setTableId(Integer tableId) {
        set("table_id", tableId);
        return this;
    }

    /**
     * 获取表名
     */
    public String getTableKey() {
        return (String) get("table_key");
    }

    /**
     * 设置表名
     */
    public TableModel setTableKey(String tableKey) {
        set("table_key", tableKey);
        return this;
    }

    /**
     * 获取显示名称
     */
    public String getTableName() {
        return (String) get("table_name");
    }

    /**
     * 设置显示名称
     */
    public TableModel setTableName(String tableName) {
        set("table_name", tableName);
        return this;
    }

    /**
     * 获取模型包名
     */
    public String getModelPackage() {
        return (String) get("model_package");
    }

    /**
     * 设置模型包名
     */
    public TableModel setModelPackage(String modelPackage) {
        set("model_package", modelPackage);
        return this;
    }

    /**
     * 获取创建时间
     */
    public Date getCreateDatetime() {
        return (Date) get("create_datetime");
    }

    /**
     * 设置创建时间
     */
    public TableModel setCreateDatetime(Date createDatetime) {
        set("create_datetime", createDatetime);
        return this;
    }

}