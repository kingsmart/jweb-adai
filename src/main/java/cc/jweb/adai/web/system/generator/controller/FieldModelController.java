/*
 * Copyright (c) 2020-2021 imlzw@vip.qq.com jweb.cc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package cc.jweb.adai.web.system.generator.controller;

import cc.jweb.adai.web.system.generator.model.FieldModel;
import cc.jweb.adai.web.system.log.service.SysLogService;
import cc.jweb.adai.web.system.sys.model.SysLog;
import cc.jweb.boot.common.lang.Result;
import cc.jweb.boot.controller.JwebController;
import cc.jweb.boot.db.Db;
import cc.jweb.boot.utils.lang.StringUtils;
import io.jboot.web.controller.annotation.RequestMapping;
import cc.jweb.boot.security.annotation.RequiresPermissions;

import java.util.Date;
import java.util.Map;

@RequestMapping(value = "/generator/field", viewPath = "/WEB-INF/views/cc/jweb/web/generator/gid_2")
public class FieldModelController extends JwebController {

    public void index() {
        render("index.html");
    }

    /**
     * 编辑页面
     */
    @RequiresPermissions("system:generator:field:edit")
    public void editPage() {
        String id = getPara("field_id");
        FieldModel fieldModel = (FieldModel) FieldModel.dao.findById(id);
        if (fieldModel == null) {
            fieldModel = new FieldModel();
        }
        setAttr("detail", fieldModel);
        render("edit.html");
    }

    /**
     * 加载分页列表数据
     */
    public void list() {
        Map<String, Object> params = getPageParamsPlus();
        params.put("limit", 100);
        Object[] values = getParaValues("values");
        Result result = new Result(false, "未知异常！");
        result.set("code", 400);
        if (StringUtils.isNotBlank(values)) { // formSelects的回显接口
            params.put("field_ids", values);
            result.setListData(Db.find(Db.getSqlPara("sys_field_model.queryPageList", params)));
            result.setSuccess(true);
            result.set("code", 0);
        } else {
            result = Db.paginate("sys_field_model.queryPageList", "sys_field_model.count", params);
            result.set("count", result.get(Result.LIST_TOTAL_KEY));
            result.setSuccess(true);
            result.set("code", 0);
        }
        renderJson(result);
    }

    /**
     * 保存用户信息（新增与修改）
     */
    public void save() {
        FieldModel columnModel = getColumnModel(FieldModel.class);
        FieldModel oldModel = null;
        Object id = columnModel.get("field_id");
        if (id != null) {
            oldModel = (FieldModel) FieldModel.dao.findById(id);
        }
        if (oldModel != null) { // 编辑
            columnModel.update();
        } else { // 新增
            columnModel.set("create_datetime", new Date());
            columnModel.save();
        }
        SysLogService.service.setSyslog(SysLog.STATUS_SUCCESS ," 保存表模型【" + columnModel.getFieldName()+ "】成功！");
        renderJson(new Result(true, "保存表模型信息成功！"));
    }

    /**
     * 删除记录
     */
    public void delete() {
        String[] ids = getParaValues("ids");
        boolean b = FieldModel.dao.deleteById(ids);
        SysLogService.service.setSyslog(b ? SysLog.STATUS_SUCCESS : SysLog.STATUS_FAILURE, "删除表模型【id:" + StringUtils.join(ids, ",") + "】" + (b ? "成功" : "失败") + " !");
        renderJson(new Result(b, b ? "删除成功！" : "删除失败！"));
    }

}