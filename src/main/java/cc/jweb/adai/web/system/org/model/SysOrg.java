/*
 * Copyright (c) 2020-2021 imlzw@vip.qq.com jweb.cc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package cc.jweb.adai.web.system.org.model;

import cc.jweb.boot.annotation.Column;
import io.jboot.db.annotation.Table;

import java.util.Date;

/**
 * 组织机构
 * 该模型代码由代码生成器自动生成
 *
 * @generateDate 2020-09-27 17:45:47
 * @since 1.0
 * @author imlzw
 * @source 阿呆极速开发平台 adai.imlzw.cn
 */
@Table(tableName = "sys_org", primaryKey = "org_id")
public class SysOrg extends com.jfinal.plugin.activerecord.Model<SysOrg> {
	private static final long serialVersionUID = -1601199947533L;
	public static final SysOrg dao = new SysOrg().dao();
    // 自动生成模型字段列表

    // 排序号
    @Column(field="order_no")
    private Integer orderNo;

    // 创建时间
    @Column(field="create_datetime")
    private Date createDatetime;

    // 机构名称
    @Column(field="org_name")
    private String orgName;

    // 机构代码
    @Column(field="org_code")
    private String orgCode;

    // 上级机构
    @Column(field="org_pid")
    private Integer orgPid;

    // 机构编号
    @Column(field="org_id")
    private Integer orgId;

    // 自动生成getter与setter方法

    /**
     *  获取排序号
     */
    public Integer getOrderNo(){
        return (Integer)get("order_no");
    }

    /**
     *  设置排序号
     */
    public SysOrg setOrderNo(Integer orderNo) {
        set("order_no", orderNo);
        return this;
    }

    /**
     *  获取创建时间
     */
    public Date getCreateDatetime(){
        return (Date)get("create_datetime");
    }

    /**
     *  设置创建时间
     */
    public SysOrg setCreateDatetime(Date createDatetime) {
        set("create_datetime", createDatetime);
        return this;
    }

    /**
     *  获取机构名称
     */
    public String getOrgName(){
        return (String)get("org_name");
    }

    /**
     *  设置机构名称
     */
    public SysOrg setOrgName(String orgName) {
        set("org_name", orgName);
        return this;
    }

    /**
     *  获取机构代码
     */
    public String getOrgCode(){
        return (String)get("org_code");
    }

    /**
     *  设置机构代码
     */
    public SysOrg setOrgCode(String orgCode) {
        set("org_code", orgCode);
        return this;
    }

    /**
     *  获取上级机构
     */
    public Integer getOrgPid(){
        return (Integer)get("org_pid");
    }

    /**
     *  设置上级机构
     */
    public SysOrg setOrgPid(Integer orgPid) {
        set("org_pid", orgPid);
        return this;
    }

    /**
     *  获取机构编号
     */
    public Integer getOrgId(){
        return (Integer)get("org_id");
    }

    /**
     *  设置机构编号
     */
    public SysOrg setOrgId(Integer orgId) {
        set("org_id", orgId);
        return this;
    }

}