/*
 * Copyright (c) 2020-2021 imlzw@vip.qq.com jweb.cc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package cc.jweb.adai.web.system.generator.model.code;

import cc.jweb.adai.web.system.generator.model.TemplateModel;
import cc.jweb.boot.utils.gson.GsonUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * 生成器配置
 */
public class GeneratorConfig {

    private String ctlUri; //控制层URI
    private String packagePath; //包路径

    private TemplateModel template;// 模板
    private List<TableCodeModel> tables = new ArrayList<>(4); // 表模型列表
    private List<Map<String, Object>> releaseFieldsList = new ArrayList<Map<String, Object>>();// 表关联配置列表

    public String getCtlUri() {
        return ctlUri;
    }

    public GeneratorConfig setCtlUri(String ctlUri) {
        this.ctlUri = ctlUri;
        return this;
    }

    public String getPackagePath() {
        return packagePath;
    }

    public GeneratorConfig setPackagePath(String packagePath) {
        this.packagePath = packagePath;
        return this;
    }

    public TemplateModel getTemplate() {
        return template;
    }

    public GeneratorConfig setTemplate(TemplateModel template) {
        this.template = template;
        return this;
    }

    public List<TableCodeModel> getTables() {
        return tables;
    }

    public GeneratorConfig setTables(List<TableCodeModel> tables) {
        this.tables = tables;
        return this;
    }

    public GeneratorConfig addTable(TableCodeModel table) {
        tables.add(table);
        return this;
    }

    public List<Map<String, Object>> getReleaseFieldsList() {
        return releaseFieldsList;
    }

    public GeneratorConfig setReleaseFieldsList(List<Map<String, Object>> releaseFieldsList) {
        this.releaseFieldsList = releaseFieldsList;
        return this;
    }

    /**
     * 获取表关联关系对应表的关键字段
     *
     * @param tableFromIndex
     * @param tableToIndex
     * @param fieldTableIndex
     * @return
     */
    public String getTableReleaseField(int tableFromIndex, int tableToIndex, int fieldTableIndex) {
        for (Map<String, Object> stringObjectMap : releaseFieldsList) {
            String releaseField1 = stringObjectMap.get("releaseField1").toString();
            String releaseField2 = stringObjectMap.get("releaseField2").toString();
            if ((releaseField1.indexOf(tableFromIndex + ".") == 0 && releaseField2.indexOf(tableToIndex + ".") == 0)) {
                if (fieldTableIndex == tableFromIndex) {
                    return releaseField1.split("\\.")[1];
                }
                if (fieldTableIndex == tableToIndex) {
                    return releaseField2.split("\\.")[1];
                }
            }
            if ((releaseField2.indexOf(tableFromIndex + ".") == 0 && releaseField1.indexOf(tableToIndex + ".") == 0)) {
                if (fieldTableIndex == tableFromIndex) {
                    return releaseField2.split("\\.")[1];
                }
                if (fieldTableIndex == tableToIndex) {
                    return releaseField1.split("\\.")[1];
                }
            }
        }
        return null;
    }

    public String toJson() {
        return GsonUtils.get().toJson(this);
    }

    public String getReleaseFieldsListJson() {
        return GsonUtils.get().toJson(this.releaseFieldsList);
    }
}
