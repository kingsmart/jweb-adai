/*
 * Copyright (c) 2020-2021 imlzw@vip.qq.com jweb.cc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package cc.jweb.adai.web.system.role.controller;

import cc.jweb.adai.web.system.log.service.SysLogService;
import cc.jweb.adai.web.system.role.model.SysRoleOper;
import cc.jweb.adai.web.system.sys.model.SysLog;
import cc.jweb.boot.common.lang.Result;
import cc.jweb.boot.controller.JwebController;
import cc.jweb.boot.db.Db;
import cc.jweb.boot.utils.lang.IDGenerator;
import cc.jweb.boot.utils.lang.StringUtils;
import com.jfinal.core.ActionKey;
import cc.jweb.adai.web.system.generator.utils.GeneratorUtils;
import io.jboot.web.controller.annotation.RequestMapping;
import cc.jweb.boot.security.annotation.RequiresPermissions;

import java.util.Map;

/**
 * 角色权限
 * 该控制器代码由阿呆代码生成器自动生成
 *
 * @author imlzw
 * @generateDate 2020-09-30 15:15:11
 * @source 阿呆极速开发平台 adai.imlzw.cn
 * @since 1.0
 */
@RequiresPermissions("system:role:view")
@RequestMapping(value = "/system/role/table3", viewPath = "/WEB-INF/views/cc/jweb/web/system/role/gid_17")
public class Table3SysRoleOperController extends JwebController {

    public void index() {
        render("index.html");
    }

    /**
     * 编辑页面
     */
    public void editPage() {
        String id = getPara("ro_id");
        SysRoleOper sysRoleOper = null;
        if (id != null) {
            sysRoleOper = SysRoleOper.dao.findById(id);
            if (sysRoleOper == null) {
                sysRoleOper = new SysRoleOper();
            }
        }
        if (sysRoleOper == null) {
            sysRoleOper = new SysRoleOper();
        }
        setAttr("detail", sysRoleOper);
        keepPara();
        render("table3edit.html");
    }

    /**
     * 加载分页列表数据
     */
    public void list() {
        Map<String, Object> params = getPageParamsPlus();
        params.put("_filters_", GeneratorUtils.parseFilterParams(params));
        Object[] values = getParaValues("values");
        Result result = new Result(false, "未知异常！");
        result.set("code", 400);
        if (StringUtils.isNotBlank(values)) { // formSelects的回显接口
            params.put("ro_ids", values);
            result.setListData(Db.find(Db.getSqlPara("gid-17-tno-3-cc.jweb.adai.web.system.role.sys_role_oper.queryPageList", params)));
            result.setSuccess(true);
            result.set("code", 0);
        } else {
            result = Db.paginate("gid-17-tno-3-cc.jweb.adai.web.system.role.sys_role_oper.queryPageList", "gid-17-tno-3-cc.jweb.adai.web.system.role.sys_role_oper.count", params);
            result.set("count", result.get(Result.LIST_TOTAL_KEY));
            result.setSuccess(true);
            result.set("code", 0);
        }
        renderJson(result);
    }

    /**
     * 保存用户信息（新增与修改）
     */
    public void save() {
        SysRoleOper columnModel = getColumnModel(SysRoleOper.class);
        SysRoleOper oldModel = null;
        Object id = columnModel.get("ro_id");
        if (id != null) {
            oldModel = SysRoleOper.dao.findById(id);
        }
        if (oldModel != null) { // 编辑
            columnModel.update();
        } else { // 新增
            columnModel.setRoId(IDGenerator.nextId("SysRoleOper", IDGenerator.TYPE_SNOWFLAKE));
            columnModel.save();
        }
        SysLogService.service.setSyslog("角色人员权限", SysLog.STATUS_SUCCESS ,"绑定角色权限信息【roleId:" + columnModel.getRoleId()+ "】成功！");
        renderJson(new Result(true, "保存角色权限信息成功！"));
    }

    /**
     * 保存操作权限
     */
    public void saveOpers() {
        String roleId = getPara("roleId");
        if (roleId == null) {
            renderJson(new Result(false, "未指定角色！"));
            return;
        }
        String[] operIds = getParaValues("operId");
        boolean tx = false;
        if (operIds != null) {
            tx = Db.tx(() -> {
                int update = Db.update("delete from sys_role_oper where role_id = ?", roleId);
                for (String operId : operIds) {
                    SysRoleOper sysRoleOper = new SysRoleOper();
                    sysRoleOper.setRoId(IDGenerator.nextId("SysRoleOper", IDGenerator.TYPE_SNOWFLAKE));
                    sysRoleOper.setOperId(Integer.parseInt(operId));
                    sysRoleOper.setRoleId(Integer.parseInt(roleId));
                    sysRoleOper.save();
                }
                return true;
            });
        } else {
            tx = true;
        }
        SysLogService.service.setSyslog("角色人员权限", SysLog.STATUS_SUCCESS ,"绑定角色权限信息【roleId:" + roleId + "】成功！");
        renderJson(new Result(tx, tx ? "保存角色权限信息成功！" : "保存角色权限信息失败！"));
    }

    /**
     * 删除记录
     */
    public void delete() {
        String[] ids = getParaValues("ids");
        if (ids == null || ids.length <= 0) {
            renderJson(new Result(true, "删除成功！"));
            return;
        }
        boolean b = true;
        for (String id : ids) {
            b = b & SysRoleOper.dao.deleteById(id);
        }
        SysLogService.service.setSyslog("角色人员权限", b ? SysLog.STATUS_SUCCESS : SysLog.STATUS_FAILURE, "删除角色权限信息【id:" + StringUtils.join(ids, ",") + "】" + (b ? "成功" : "失败") + " !");
        renderJson(new Result(b, b ? "删除成功！" : "删除失败！"));
    }

    // ---- 字段关联接口 ----

    /**
     * role_id 字段关联表列表接口
     */
    @ActionKey("/system/role/table3/role_id/sys_role/list")
    public void roleIdSysRoleList() {
        Map<String, Object> params = getPageParamsPlus();
        params.put("_filters_", GeneratorUtils.parseFilterParams(params));
        Object[] values = getParaValues("values");
        Result result = new Result(false, "未知异常！");
        result.set("code", 400);
        if (StringUtils.isNotBlank(values)) { // formSelects的回显接口
            params.put("role_ids", values);
            result.setListData(Db.find(Db.getSqlPara("gid-17-tno-3-cc.jweb.adai.web.system.role.sys_role_oper.role_id_sys_role_list", params)));
            result.setSuccess(true);
            result.set("code", 0);
        } else {
            result = Db.paginate("gid-17-tno-3-cc.jweb.adai.web.system.role.sys_role_oper.role_id_sys_role_list", "gid-17-tno-3-cc.jweb.adai.web.system.role.sys_role_oper.role_id_sys_role_count", params);
            result.set("count", result.get(Result.LIST_TOTAL_KEY));
            result.setSuccess(true);
            result.set("code", 0);
        }
        renderJson(result);
    }

    /**
     * oper_id 字段关联表列表接口
     */
    @ActionKey("/system/role/table3/oper_id/sql/list")
    public void operIdSqlList() {
        Map<String, Object> params = getPageParamsPlus();
        params.put("_filters_", GeneratorUtils.parseFilterParams(params));
        Object[] values = getParaValues("values");
        Result result = new Result(false, "未知异常！");
        result.set("code", 400);
        if (StringUtils.isNotBlank(values)) { // formSelects的回显接口
            params.put("oper_ids", values);
            result.setListData(Db.find(Db.getSqlPara("gid-17-tno-3-cc.jweb.adai.web.system.role.sys_role_oper.oper_id_sql_list", params)));
            result.setSuccess(true);
            result.set("code", 0);
        } else {
            result = Db.paginate("gid-17-tno-3-cc.jweb.adai.web.system.role.sys_role_oper.oper_id_sql_list", "gid-17-tno-3-cc.jweb.adai.web.system.role.sys_role_oper.oper_id_sql_count", params);
            result.set("count", result.get(Result.LIST_TOTAL_KEY));
            result.setSuccess(true);
            result.set("code", 0);
        }
        renderJson(result);
    }

}