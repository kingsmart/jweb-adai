/*
 * Copyright (c) 2020-2021 imlzw@vip.qq.com jweb.cc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package cc.jweb.adai.web.system.generator.utils;

import cc.jweb.adai.web.system.generator.model.FieldModel;
import cc.jweb.adai.web.system.generator.model.code.FieldCodeModel;

import java.util.List;
import java.util.Optional;

/**
 * 模型工具类
 *
 * @author 47475
 */
public class ModelUtils {

    /**
     * 查找字段列表中的主键
     *
     * @param fieldModels
     * @return
     */
    public static String findPrimaryKey(List<? extends FieldModel> fieldModels) {
        Optional<String> reduce = fieldModels.stream()
                .filter(fieldModel -> fieldModel.isPrimary())
                .map(fieldModel -> fieldModel.getFieldKey())
                .reduce((s, s2) -> s + (s2 != null ? "," + s2 : ""));
        if (reduce.isPresent()) {
            return reduce.get();
        } else {
            return null;
        }
    }

    /**
     * 查找字段列表中的标题字段，为空则返回主键字段
     *
     * @param fieldModels
     * @return
     */
    public static FieldCodeModel findPrimaryField(List<? extends FieldCodeModel> fieldModels) {
        for (FieldCodeModel fieldModel : fieldModels) {
            if (fieldModel.isPrimary()) {
                return fieldModel;
            }
        }
        return findPrimaryField(fieldModels);
    }

    /**
     * 查找字段列表中的标题字段
     *
     * @param fieldModels
     * @return
     */
    public static FieldCodeModel findTitleField(List<? extends FieldCodeModel> fieldModels) {
        for (FieldCodeModel fieldModel : fieldModels) {
            if (fieldModel.isTitle()) {
                return fieldModel;
            }
        }
        return findPrimaryField(fieldModels);
    }

    /**
     * 根据字段模型获取java对应的类型
     *
     * @param fieldModel
     * @return
     */
    public static String getJavaType(FieldModel fieldModel) {
        String javaType = "Object";
        String fieldType = fieldModel.getFieldType();
        if (fieldType == null) {
            return javaType;
        }
        switch (fieldType) {
            case "varchar":
            case "text":
            case "tinytext":
            case "longtext":
            case "json":
            case "char":
            case "string":
                javaType = "String";
                break;
            case "int":
                javaType = "Integer";
                break;
            case "tinyint":
                if (fieldModel.getFieldLength() == 1) {
                    javaType = "Boolean";
                } else {
                    javaType = "Integer";
                }
                break;
            case "bit":
                javaType = "Boolean";
                break;
            case "bigint":
                javaType = "Long";
                break;
            case "float":
                javaType = "Float";
                break;
            case "double":
                javaType = "Double";
                break;
            case "decimal":
                javaType = "BigDecimal";
                break;
            case "date":
            case "datetime":
            case "timestamp":
                javaType = "Date";
                break;
            default:
                javaType = "Object";
        }
        return javaType;
    }
}