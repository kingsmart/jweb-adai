/*
 * Copyright (c) 2020-2021 imlzw@vip.qq.com jweb.cc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package cc.jweb.adai.web.system.generator.model.vo;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.*;

/**
 * 配置类
 *
 * @author linzhiwei
 */
public class Config {
    //数据处理配置对象
    private Map<String, ConfigElement> config;

    private static Gson gson = new Gson();

    private static final Config EMPTY_CONFIG = new Config();

    /**
     * 默认构造函数
     */
    public Config() {
        config = new LinkedHashMap<String, ConfigElement>();
    }

    /**
     * 默认构造函数
     *
     * @param config
     */
    public Config(Map<String, ConfigElement> config) {
        this.config = config;
        if (config != null) {
            config = new LinkedHashMap<String, ConfigElement>();
        }
    }

    /**
     * 获取配置信息
     *
     * @param configName
     * @return
     */
    public ConfigElement getConfigElement(String configName) {
        if (configName != null && configName.trim().length() > 0) {
            return config.get(configName);
        }
        return null;
    }

    /**
     * 获取配置信息
     *
     * @param configName
     * @return
     */
    public String getConfig(String configName) {
        ConfigElement configElement = getConfigElement(configName);
        if (configElement != null) {
            return configElement.getConfigValue();
        }
        return null;
    }

    /**
     * 设置配置信息
     *
     * @param configElement
     * @return
     */
    public void addConfig(ConfigElement configElement) {
        if (configElement != null && configElement.getConfigName() != null && configElement.getConfigName().trim().length() > 0) {
            config.put(configElement.getConfigName(), configElement);
        }
    }

    public List<ConfigElement> getAllConfigElement() {
        List<ConfigElement> list = new ArrayList<ConfigElement>();
        if (config != null) {
            for (Iterator<String> itor = config.keySet().iterator(); itor.hasNext(); ) {
                list.add(config.get(itor.next()));
            }
        }
        return list;
    }

    /**
     * json转Config对象
     *
     * @param configJson
     * @return
     */
    public static Config fromJson(String configJson) {
        if (configJson != null && configJson.trim().length() > 0) {
            try {
                return (Config) gson.fromJson(configJson, new TypeToken<Config>() {
                }.getType());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return EMPTY_CONFIG;
    }

    /**
     * Config对象转json
     *
     * @param config
     * @return
     */
    public static String toJson(Config config) {
        if (config != null) {
            try {
                return gson.toJson(config);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return null;
    }
}
