/*
 * Copyright (c) 2020-2021 imlzw@vip.qq.com jweb.cc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package cc.jweb.adai.web.system.org.model;

import cc.jweb.boot.annotation.Column;
import com.jfinal.plugin.activerecord.IBean;
import io.jboot.db.annotation.Table;
import io.jboot.db.model.JbootModel;

import java.util.Date;

/**
 * 用户
 * 该模型代码由代码生成器自动生成
 *
 * @author imlzw
 * @generateDate 2020-09-27 17:57:23
 * @source 阿呆极速开发平台 adai.imlzw.cn
 * @since 1.0
 */
@Table(tableName = "sys_user", primaryKey = "user_id")
public class SysUser extends JbootModel<SysUser> implements IBean {
    public static final SysUser dao = new SysUser().dao();
    private static final long serialVersionUID = -1601200643422L;
    // 自动生成模型字段列表
    // 最近登录
    @Column(field = "user_last_login_datetime")
    private Date userLastLoginDatetime;

    // 创建时间
    @Column(field = "create_datetime")
    private Date createDatetime;

    // 用户状态
    @Column(field = "user_status")
    private Integer userStatus;

    // 电子邮箱
    @Column(field = "user_email")
    private String userEmail;

    // 手机号
    @Column(field = "user_phone")
    private String userPhone;

    // 登录密码
    @Column(field = "user_password")
    private String userPassword;

    // 登录账号
    @Column(field = "user_account")
    private String userAccount;

    // 用户姓名
    @Column(field = "user_name")
    private String userName;

    // 所属机构
    @Column(field = "org_id")
    private Integer orgId;

    // 用户编号
    @Column(field = "user_id")
    private Integer userId;

    // 性别
    @Column(field = "user_sex")
    private Integer userSex;

    // 自动生成getter与setter方法

    /**
     * 获取最近登录
     */
    public Date getUserLastLoginDatetime() {
        return (Date) get("user_last_login_datetime");
    }

    /**
     * 设置最近登录
     */
    public SysUser setUserLastLoginDatetime(Date userLastLoginDatetime) {
        set("user_last_login_datetime", userLastLoginDatetime);
        return this;
    }

    /**
     * 获取创建时间
     */
    public Date getCreateDatetime() {
        return (Date) get("create_datetime");
    }

    /**
     * 设置创建时间
     */
    public SysUser setCreateDatetime(Date createDatetime) {
        set("create_datetime", createDatetime);
        return this;
    }

    /**
     * 获取用户状态
     */
    public Integer getUserStatus() {
        return (Integer) get("user_status");
    }

    /**
     * 设置用户状态
     */
    public SysUser setUserStatus(Integer userStatus) {
        set("user_status", userStatus);
        return this;
    }

    /**
     * 获取电子邮箱
     */
    public String getUserEmail() {
        return (String) get("user_email");
    }

    /**
     * 设置电子邮箱
     */
    public SysUser setUserEmail(String userEmail) {
        set("user_email", userEmail);
        return this;
    }

    /**
     * 获取手机号
     */
    public String getUserPhone() {
        return (String) get("user_phone");
    }

    /**
     * 设置手机号
     */
    public SysUser setUserPhone(String userPhone) {
        set("user_phone", userPhone);
        return this;
    }

    /**
     * 获取登录密码
     */
    public String getUserPassword() {
        return (String) get("user_password");
    }

    /**
     * 设置登录密码
     */
    public SysUser setUserPassword(String userPassword) {
        set("user_password", userPassword);
        return this;
    }

    /**
     * 获取登录账号
     */
    public String getUserAccount() {
        return (String) get("user_account");
    }

    /**
     * 设置登录账号
     */
    public SysUser setUserAccount(String userAccount) {
        set("user_account", userAccount);
        return this;
    }

    /**
     * 获取用户姓名
     */
    public String getUserName() {
        return (String) get("user_name");
    }

    /**
     * 设置用户姓名
     */
    public SysUser setUserName(String userName) {
        set("user_name", userName);
        return this;
    }

    /**
     * 获取所属机构
     */
    public Integer getOrgId() {
        return (Integer) get("org_id");
    }

    /**
     * 设置所属机构
     */
    public SysUser setOrgId(Integer orgId) {
        set("org_id", orgId);
        return this;
    }

    /**
     * 获取用户编号
     */
    public Integer getUserId() {
        return (Integer) get("user_id");
    }

    /**
     * 设置用户编号
     */
    public SysUser setUserId(Integer userId) {
        set("user_id", userId);
        return this;
    }

    /**
     * 获取性别
     */
    public Integer getUserSex() {
        return (Integer) get("user_sex");
    }

    /**
     * 设置性别
     */
    public SysUser setUserSex(Integer userSex) {
        set("user_sex", userSex);
        return this;
    }

}