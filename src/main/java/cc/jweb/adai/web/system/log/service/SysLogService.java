/*
 * Copyright (c) 2020-2021 imlzw@vip.qq.com jweb.cc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package cc.jweb.adai.web.system.log.service;

import cc.jweb.adai.web.system.sys.model.SysLog;
import cc.jweb.boot.common.lang.Result;
import com.jfinal.aop.Aop;

public class SysLogService {
    public static final SysLogService service = Aop.get(SysLogService.class);

    // 日志线程变量
    public static ThreadLocal<SysLog> LOG_THREAD_LOCAL = new ThreadLocal<SysLog>();

    /**
     * 设置系统日志
     *
     */
    public void setSyslog(int status, String content) {
        setSyslog(null, status, content);
    }

    /**
     * 设置系统日志
     *
     */
    public void setSyslog(String category, int status, String content) {
        SysLog sysLog = LOG_THREAD_LOCAL.get();
        if (sysLog == null) {
            sysLog = new SysLog();
            LOG_THREAD_LOCAL.set(sysLog);
        }
        sysLog.setLogCategory(category);
        sysLog.setLogStatus(status);
        sysLog.setLogContent(content);
    }

    /**
     * 获取系统日志
     *
     * @return
     */
    public SysLog getSyslog() {
        return LOG_THREAD_LOCAL.get();
    }

    /**
     * 设置系统日志
     *
     * @param result
     */
    public void setSyslog(Result result) {
        setSyslog(result.isSuccess() ? SysLog.STATUS_SUCCESS : SysLog.STATUS_FAILURE, result.getMessage());
    }

    public void setSyslog(SysLog syslog) {
        LOG_THREAD_LOCAL.set(syslog);
    }

    public void clearSyslog() {
        LOG_THREAD_LOCAL.remove();
    }


}
